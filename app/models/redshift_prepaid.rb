class RedshiftPrepaid < RedshiftBase
  self.table_name = :prepaidcode

  @table = RedshiftCleanup::Settings.table_names.prepaidcode
  @primary_keys = "
    prepaid_code_id,
    hotel_id,
    time_limit,
    code,
    cost,
    data_limit,
    first_used,
    speed_up_limit,
    type,
    speed_down_limit
  "

  @identity_key = "prepaid_code_id"

  @temp_table = "#{@table}_temp"
  @old_table = "#{@table}_old"

  QUERY_TMPL = <<-EOT
  CREATE TABLE hotel_temp (LIKE hotel);
  BEGIN;
  INSERT INTO hotel_temp (SELECT status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory FROM hotel GROUP BY status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory order by hotel_id asc);
  ALTER TABLE hotel RENAME TO hotel_old;
  ALTER TABLE hotel_temp RENAME TO hotel;
  COMMIT;
  EOT

  def self.cleanup
    # puts QUERY_TMPL
    puts Time.now
    self.find_by_sql("CREATE TABLE IF NOT EXISTS #{@temp_table} (LIKE #{@table});")
    self.find_by_sql("DELETE #{@temp_table};")
    puts "#{@temp_table} created"
    self.find_by_sql("INSERT INTO #{@temp_table} SELECT #{@primary_keys} FROM #{@table} GROUP BY #{@primary_keys} HAVING COUNT(*) > 1;")
    puts Time.now
    ndups = self.count_by_sql("SELECT COUNT(*) FROM #{@temp_table};")
    if (ndups > 0)
      puts "#{ndups} duplicates copied"
      self.find_by_sql("DELETE FROM #{@table} USING (SELECT * FROM #{@temp_table})s 
        WHERE s.prepaid_code_id=#{@table}.prepaid_code_id AND
              s.hotel_id=#{@table}.hotel_id AND
              (s.time_limit=#{@table}.time_limit or (s.time_limit is null AND #{@table}.time_limit is null)) AND
              (s.code=#{@table}.code or (s.code is null AND #{@table}.code is null)) AND
              s.cost=#{@table}.cost AND
              (s.data_limit=#{@table}.data_limit or (s.data_limit is null AND #{@table}.data_limit is null)) AND
              (s.first_used=#{@table}.first_used or (s.first_used is null AND #{@table}.first_used is null)) AND
              (s.speed_up_limit=#{@table}.speed_up_limit or (s.speed_up_limit is null AND #{@table}.speed_up_limit is null)) AND
              (s.type=#{@table}.type or (s.type is null AND #{@table}.type is null)) AND
              (s.speed_down_limit=#{@table}.speed_down_limit or (s.speed_down_limit is null AND #{@table}.speed_down_limit is null))
      ;")
      puts Time.now
      self.find_by_sql("INSERT INTO #{@table}
                        SELECT * from #{@temp_table};")
      puts "unique records re-inserted to #{@table}"

    else
      puts "#{@table} has no duplicates"
    end
    self.find_by_sql("DROP TABLE IF EXISTS #{@temp_table};")
    puts "dropped #{@temp_table}"
    puts Time.now
  end

end
