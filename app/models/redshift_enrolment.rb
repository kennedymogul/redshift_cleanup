class RedshiftEnrolment < RedshiftBase
  self.table_name = :enrolment

  @table = RedshiftCleanup::Settings.table_names.enrolment
  @primary_keys = "
    enrolment_id,
    accounting_subcategory_name,
    bandwidth_pool_name,
    business_model,
    device_limit,
    start_time,
    finish_time,
    hard_data_limit,
    hotel_id,
    overlimit_data_cost,
    prepaid_code,
    room_name,
    room_type,
    soft_data_limit,
    speed_down_limit,
    speed_up_limit,
    time_cost    
  "

  @identity_key = "enrolment_id"

  @temp_table = "#{@table}_temp"
  @old_table = "#{@table}_old"

  QUERY_TMPL = <<-EOT
  CREATE TABLE hotel_temp (LIKE hotel);
  BEGIN;
  INSERT INTO hotel_temp (SELECT status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory FROM hotel GROUP BY status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory order by hotel_id asc);
  ALTER TABLE hotel RENAME TO hotel_old;
  ALTER TABLE hotel_temp RENAME TO hotel;
  COMMIT;
  EOT

  def self.cleanup
    # puts QUERY_TMPL
    puts Time.now
    self.find_by_sql("CREATE TABLE IF NOT EXISTS #{@temp_table} (LIKE #{@table});")
    self.find_by_sql("DELETE #{@temp_table};")
    puts "#{@temp_table} created"
    self.find_by_sql("INSERT INTO #{@temp_table} SELECT #{@primary_keys} FROM #{@table} GROUP BY #{@primary_keys} HAVING COUNT(*) > 1;")
    puts Time.now
    ndups = self.count_by_sql("SELECT COUNT(*) FROM #{@temp_table};")
    if (ndups > 0)
      puts "#{ndups} duplicates copied"
      self.find_by_sql("DELETE FROM #{@table} USING (SELECT * FROM #{@temp_table})s 
          WHERE s.hotel_id=#{@table}.hotel_id AND
                s.enrolment_id=#{@table}.enrolment_id AND
                (s.soft_data_limit=#{@table}.soft_data_limit or (s.soft_data_limit is null AND #{@table}.soft_data_limit is null) ) AND
                (s.bandwidth_pool_name=#{@table}.bandwidth_pool_name or (s.bandwidth_pool_name is null AND #{@table}.bandwidth_pool_name is null) ) AND
                (s.prepaid_code=#{@table}.prepaid_code or (s.hard_data_limit is null AND #{@table}.hard_data_limit is null) ) AND
                s.finish_time=#{@table}.finish_time AND
                s.overlimit_data_cost=#{@table}.overlimit_data_cost AND
                s.start_time=#{@table}.start_time AND
                (s.room_name=#{@table}.room_name or (s.room_name is null AND #{@table}.room_name is null) ) AND
                (s.hard_data_limit=#{@table}.hard_data_limit or (s.hard_data_limit is null AND #{@table}.hard_data_limit is null) ) AND
                (s.room_type=#{@table}.room_type or (s.room_type is null AND #{@table}.room_type is null) ) AND
                s.time_cost=#{@table}.time_cost AND
                (s.device_limit=#{@table}.device_limit or (s.device_limit is null AND #{@table}.device_limit is null) ) AND
                (s.accounting_subcategory_name=#{@table}.accounting_subcategory_name OR (s.accounting_subcategory_name is null and #{@table}.accounting_subcategory_name is null) ) AND
                (s.business_model=#{@table}.business_model OR (s.business_model is null and #{@table}.business_model is null) ) AND
                (s.speed_down_limit=#{@table}.speed_down_limit or (s.speed_down_limit is null AND #{@table}.speed_down_limit is null) ) AND
                (s.speed_up_limit=#{@table}.speed_up_limit or (s.speed_up_limit is null AND #{@table}.speed_up_limit is null))
          ; ")
      puts Time.now
      puts "records with duplicates deleted from #{@table}"
      self.find_by_sql("INSERT INTO #{@table}
                        SELECT * from #{@temp_table};")
      puts "unique records re-inserted to #{@table}"
    else
      puts "#{@table} has no duplicates"
    end
    self.find_by_sql("DROP TABLE IF EXISTS #{@temp_table};")
    puts "dropped #{@temp_table}"
    puts Time.now
  end

end
