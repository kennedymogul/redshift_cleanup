class RedshiftNetworkDevice < RedshiftBase
  self.table_name = :networkdevice

  @table = RedshiftCleanup::Settings.table_names.networkdevice
  @primary_keys = "
    hotel_id,
    networkdevice_id,
    networkdevice_type,
    mac_address,
    make,
    model,
    ip_address,
    can_access_internet,
    location_floor,
    location_description,
    firmware_version,
    support_contract_expiry,
    installation_date,
    serial_number,
    monitored,
    monitored_api,
    monitored_ping,
    monitored_snmp,
    monitored_soap,
    cabling_type,
    button_a_plan,
    button_b_plan,
    button_c_plan,
    port_count,
    is_critical
  "

  @identity_key = "billing_session_id"

  @temp_table = "#{@table}_temp"
  @old_table = "#{@table}_old"

  QUERY_TMPL = <<-EOT
  CREATE TABLE hotel_temp (LIKE hotel);
  BEGIN;
  INSERT INTO hotel_temp (SELECT status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory FROM hotel GROUP BY status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory order by hotel_id asc);
  ALTER TABLE hotel RENAME TO hotel_old;
  ALTER TABLE hotel_temp RENAME TO hotel;
  COMMIT;
  EOT

  def self.cleanup
    # puts QUERY_TMPL
    puts Time.now
    self.find_by_sql("CREATE TABLE IF NOT EXISTS #{@temp_table} (LIKE #{@table});")
    self.find_by_sql("DELETE #{@temp_table};")
    puts "#{@temp_table} created"
    self.find_by_sql("INSERT INTO #{@temp_table} SELECT #{@primary_keys} FROM #{@table} GROUP BY #{@primary_keys} HAVING COUNT(*) > 1;")
    puts Time.now
    ndups = self.count_by_sql("SELECT COUNT(*) FROM #{@temp_table};")
    if (ndups > 0)
      puts "#{ndups} duplicates copied"
      self.find_by_sql("DELETE FROM #{@table} USING (SELECT * FROM #{@temp_table})s 
          WHERE s.hotel_id=#{@table}.hotel_id AND
            s.networkdevice_id=#{@table}.networkdevice_id AND
            (s.networkdevice_type=#{@table}.networkdevice_type or (s.networkdevice_type is null AND #{@table}.networkdevice_type is null)) AND
            (s.mac_address=#{@table}.mac_address or (s.mac_address is null AND #{@table}.mac_address is null)) AND
            (s.make=#{@table}.make or (s.make is null AND #{@table}.make is null)) AND
            (s.model=#{@table}.model or (s.model is null AND #{@table}.model is null)) AND
            (s.ip_address=#{@table}.ip_address or (s.ip_address is null AND #{@table}.ip_address is null)) AND
            s.can_access_internet=#{@table}.can_access_internet AND
            (s.location_floor=#{@table}.location_floor or (s.location_floor is null AND #{@table}.location_floor is null)) AND
            (s.location_description=#{@table}.location_description or (s.location_description is null AND #{@table}.location_description is null)) AND
            (s.firmware_version=#{@table}.firmware_version or (s.firmware_version is null AND #{@table}.firmware_version is null)) AND
            (s.support_contract_expiry=#{@table}.support_contract_expiry or (s.support_contract_expiry is null AND #{@table}.support_contract_expiry is null)) AND
            (s.installation_date=#{@table}.installation_date or (s.installation_date is null AND #{@table}.installation_date is null)) AND
            (s.serial_number=#{@table}.serial_number or (s.serial_number is null AND #{@table}.serial_number is null)) AND
            s.monitored=#{@table}.monitored AND
            s.monitored_api=#{@table}.monitored_api AND
            s.monitored_ping=#{@table}.monitored_ping AND
            s.monitored_snmp=#{@table}.monitored_snmp AND
            s.monitored_soap=#{@table}.monitored_soap AND
            (s.cabling_type=#{@table}.cabling_type or (s.cabling_type is null AND #{@table}.cabling_type is null)) AND
            (s.button_a_plan=#{@table}.button_a_plan or (s.button_a_plan is null AND #{@table}.button_a_plan is null)) AND
            (s.button_b_plan=#{@table}.button_b_plan or (s.button_b_plan is null AND #{@table}.button_b_plan is null)) AND
            (s.button_c_plan=#{@table}.button_c_plan or (s.button_c_plan is null AND #{@table}.button_c_plan is null)) AND
            (s.port_count=#{@table}.port_count or (s.port_count is null AND #{@table}.port_count is null)) AND
            (s.is_critical=#{@table}.is_critical or (s.is_critical is null AND #{@table}.is_critical is null))
          ; ")
      puts Time.now
      puts "records with duplicates deleted from #{@table}"
      self.find_by_sql("INSERT INTO #{@table}
                        SELECT * from #{@temp_table};")
      puts Time.now
      puts "unique records re-inserted to #{@table}"
      
    else
      puts "#{@table} has no duplicates"
    end
    self.find_by_sql("DROP TABLE IF EXISTS #{@temp_table};")
    puts "dropped #{@temp_table}"
    puts Time.now
  end

end
