class RedshiftGuest < RedshiftBase
  self.table_name = :guest

  @table = RedshiftCleanup::Settings.table_names.guest
  @primary_keys = "
    hotel_id,
    check_in,
    check_out,
    company_name,
    email,
    group_code,
    guest_id,
    language,
    membership_level,
    membership_type,
    name,
    origin_of_booking,
    package_code,
    phone,
    rate_code,
    room_name,
    special_services,
    vip_code
  "

  @identity_key = "guest_id"

  @temp_table = "#{@table}_temp"
  @old_table = "#{@table}_old"

  QUERY_TMPL = <<-EOT
  CREATE TABLE hotel_temp (LIKE hotel);
  BEGIN;
  INSERT INTO hotel_temp (SELECT status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory FROM hotel GROUP BY status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory order by hotel_id asc);
  ALTER TABLE hotel RENAME TO hotel_old;
  ALTER TABLE hotel_temp RENAME TO hotel;
  COMMIT;
  EOT

  def self.cleanup
    
    puts Time.now
    self.find_by_sql("CREATE TABLE IF NOT EXISTS #{@temp_table} (LIKE #{@table});")
    self.find_by_sql("DELETE #{@temp_table};")
    puts "#{@temp_table} created"
    self.find_by_sql("INSERT INTO #{@temp_table} SELECT #{@primary_keys} FROM #{@table} GROUP BY #{@primary_keys} HAVING COUNT(*) > 1;")
    puts Time.now
    ndups = self.count_by_sql("SELECT COUNT(*) FROM #{@temp_table};")
    if (ndups > 0)
      puts "#{ndups} duplicates copied"
      self.find_by_sql("DELETE FROM #{@table} USING (SELECT * FROM #{@temp_table})s 
        WHERE s.hotel_id=#{@table}.hotel_id AND
              (s.check_in=#{@table}.check_in or (s.check_in is null AND #{@table}.check_in is null)) AND
              (s.check_out=#{@table}.check_out or (s.check_out is null AND #{@table}.check_out is null)) AND
              (s.company_name=#{@table}.company_name or (s.company_name is null AND #{@table}.company_name is null)) AND
              (s.email=#{@table}.email or (s.email is null AND #{@table}.email is null)) AND
              (s.group_code=#{@table}.group_code or (s.group_code is null AND #{@table}.group_code is null)) AND
              (s.guest_id=#{@table}.guest_id or (s.guest_id is null AND #{@table}.guest_id is null)) AND
              (s.language=#{@table}.language or (s.language is null AND #{@table}.language is null)) AND
              (s.membership_level=#{@table}.membership_level or (s.membership_level is null AND #{@table}.membership_level is null)) AND
              (s.membership_type=#{@table}.membership_type or (s.membership_type is null AND #{@table}.membership_type is null)) AND
              (s.name=#{@table}.name or (s.name is null AND #{@table}.name is null)) AND
              (s.origin_of_booking=#{@table}.origin_of_booking or (s.origin_of_booking is null AND #{@table}.origin_of_booking is null)) AND
              (s.package_code=#{@table}.package_code or (s.package_code is null AND #{@table}.package_code is null)) AND
              (s.phone=#{@table}.phone or (s.phone is null AND #{@table}.phone is null)) AND
              (s.rate_code=#{@table}.rate_code or (s.rate_code is null AND #{@table}.rate_code is null)) AND
              (s.room_name=#{@table}.room_name or (s.room_name is null AND #{@table}.room_name is null)) AND
              (s.special_services=#{@table}.special_services or (s.special_services is null AND #{@table}.special_services is null)) AND
              (s.vip_code=#{@table}.vip_code or (s.vip_code is null AND #{@table}.vip_code is null))
      ;")
      puts Time.now
      puts "records with duplicates deleted from #{@table}"
      self.find_by_sql("INSERT INTO #{@table}
                        SELECT * from #{@temp_table};")
      Time.now
      puts "unique records re-inserted to #{@table}"
    else
      puts "#{@table} has no duplicates"
    end
    self.find_by_sql("DROP TABLE IF EXISTS #{@temp_table};")
    puts "dropped #{@temp_table}"
    puts Time.now
  end

end
