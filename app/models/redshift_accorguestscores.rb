class RedshiftAccorguestscores < RedshiftBase
  self.table_name = :accorguestscores

  @table = RedshiftCleanup::Settings.table_names.accorguestscores
  @primary_keys = "
    hotel_id,
    date,
    average_score,
    top_score,
    score_count
  "

  @identity_key = "hotel_id"

  @temp_table = "#{@table}_temp"
  @old_table = "#{@table}_old"

  QUERY_TMPL = <<-EOT
  CREATE TABLE hotel_temp (LIKE hotel);
  BEGIN;
  INSERT INTO hotel_temp (SELECT status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory FROM hotel GROUP BY status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory order by hotel_id asc);
  ALTER TABLE hotel RENAME TO hotel_old;
  ALTER TABLE hotel_temp RENAME TO hotel;
  COMMIT;
  EOT

  def self.cleanup
    
    puts Time.now
    self.find_by_sql("CREATE TABLE IF NOT EXISTS #{@temp_table} (LIKE #{@table});")
    self.find_by_sql("DELETE #{@temp_table};")
    puts "#{@temp_table} created"
    self.find_by_sql("INSERT INTO #{@temp_table} SELECT #{@primary_keys} FROM #{@table} GROUP BY #{@primary_keys} HAVING COUNT(*) > 1;")
    puts Time.now
    ndups = self.count_by_sql("SELECT COUNT(*) FROM #{@temp_table};")
    if (ndups > 0)
      puts "#{ndups} duplicates copied"
      self.find_by_sql("DELETE FROM #{@table} USING (SELECT * FROM #{@temp_table})s 
        WHERE s.hotel_id=#{@table}.hotel_id AND
              (s.date=#{@table}.date or (s.date is null AND #{@table}.date is null)) AND
              (s.average_score=#{@table}.average_score or (s.average_score is null AND #{@table}.average_score is null)) AND
              (s.top_score=#{@table}.top_score or (s.top_score is null AND #{@table}.top_score is null)) AND
              (s.score_count=#{@table}.score_count or (s.score_count is null AND #{@table}.score_count is null))
      ;")
      puts Time.now
      puts "records with duplicates deleted from #{@table}"
      self.find_by_sql("INSERT INTO #{@table}
                        SELECT * from #{@temp_table};")
      Time.now
      puts "unique records re-inserted to #{@table}"
    else
      puts "#{@table} has no duplicates"
    end
    self.find_by_sql("DROP TABLE IF EXISTS #{@temp_table};")
    puts "dropped #{@temp_table}"
    puts Time.now
  end

end
