class RedshiftHotel < RedshiftBase
  self.table_name = :hotel

  @table = RedshiftCleanup::Settings.table_names.hotel
  @select_keys = "
    status,
    name,
    isnull(country,'') as country,
    hotel_id,
    last_infrastructure_upgrade,
    isnull(hotel_group,'') as hotel_group,
    isnull(support_territory,'') as support_territory,
    isnull(city, '') as city,
    conference_room_count,
    guest_room_count,
    isnull(hoteL_brand,'') as hoteL_brand,
    last_updated,
    room_count,
    star_rating,
    isnull(state,'') as state,
    isnull(reivernet_reference_id,'') as reivernet_reference_id,
    isnull(customer_reference_id,'') as customer_reference_id
  "

  @group_by_keys = "
    status,
    name,
    isnull(country,''),
    hotel_id,
    last_infrastructure_upgrade,
    isnull(hotel_group,''),
    isnull(support_territory,''),
    isnull(city, ''),
    conference_room_count,
    guest_room_count,
    isnull(hoteL_brand,''),
    last_updated,
    room_count,
    star_rating,
    isnull(state,''),
    isnull(reivernet_reference_id,''),
    isnull(customer_reference_id,'')
  "

  @identity_key = "hotel_id"

  @temp_table = "#{@table}_temp"
  @old_table = "#{@table}_old"

  QUERY_TMPL = <<-EOT
  CREATE TABLE hotel_temp (LIKE hotel);
  BEGIN;
  INSERT INTO hotel_temp (SELECT status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory FROM hotel GROUP BY status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory order by hotel_id asc);
  ALTER TABLE hotel RENAME TO hotel_old;
  ALTER TABLE hotel_temp RENAME TO hotel;
  COMMIT;
  EOT

  def self.cleanup
    self.dedup
    self.get_updated
  end

  def self.dedup
    
    puts Time.now, "START: dedup"
    self.find_by_sql("DROP TABLE IF EXISTS #{@temp_table}")
    self.find_by_sql("CREATE TABLE IF NOT EXISTS #{@temp_table} (LIKE #{@table});")
    self.find_by_sql("DELETE #{@temp_table};")
    puts "#{@temp_table} created"
    self.find_by_sql("INSERT INTO #{@temp_table} SELECT #{@select_keys} FROM #{@table} GROUP BY #{@group_by_keys} HAVING COUNT(*) > 1;")
    puts Time.now
    ndups = self.count_by_sql("SELECT COUNT(*) FROM #{@temp_table};")
    if (ndups > 0)
      puts "#{ndups} duplicates copied"
      self.find_by_sql("DELETE FROM #{@table} USING (SELECT * FROM #{@temp_table})s 
        WHERE (s.status=#{@table}.status or (s.status is null AND #{@table}.status is null)) AND
              (s.name=#{@table}.name or (s.name is null AND #{@table}.name is null)) AND
              (isnull(s.country,'') = isnull(#{@table}.country,'')) AND
              (s.hotel_id=#{@table}.hotel_id or (s.hotel_id is null AND #{@table}.hotel_id is null)) AND
              (s.last_infrastructure_upgrade=#{@table}.last_infrastructure_upgrade or (s.last_infrastructure_upgrade is null AND #{@table}.last_infrastructure_upgrade is null)) AND
              (isnull(s.hotel_group,'') = isnull(#{@table}.hotel_group,'')) AND
              (isnull(s.support_territory,'') = isnull(#{@table}.support_territory,'')) AND
              (isnull(s.city,'') = isnull(#{@table}.city,'')) AND
              (s.conference_room_count=#{@table}.conference_room_count or (s.conference_room_count is null AND #{@table}.conference_room_count is null)) AND
              (s.guest_room_count=#{@table}.guest_room_count or (s.guest_room_count is null AND #{@table}.guest_room_count is null)) AND
              (isnull(s.hoteL_brand,'') =isnull(#{@table}.hoteL_brand,'')) AND
              (s.last_updated=#{@table}.last_updated or (s.last_updated is null AND #{@table}.last_updated is null)) AND
              (s.room_count=#{@table}.room_count or (s.room_count is null AND #{@table}.room_count is null)) AND
              (s.star_rating=#{@table}.star_rating or (s.star_rating is null AND #{@table}.star_rating is null)) AND
              (isnull(s.state,'') = isnull(#{@table}.state,'')) AND
              (isnull(s.reivernet_reference_id,'') = isnull(#{@table}.reivernet_reference_id,'')) AND
              (isnull(s.customer_reference_id,'') = isnull(#{@table}.customer_reference_id,''))
      ;")
      puts Time.now
      puts "records with duplicates deleted from #{@table}"
      self.find_by_sql("INSERT INTO #{@table}
                        SELECT * from #{@temp_table};")
      Time.now
      puts "unique records re-inserted to #{@table}"
    else
      puts "#{@table} has no duplicates"
    end
    self.find_by_sql("DROP TABLE IF EXISTS #{@temp_table};")
    puts "dropped #{@temp_table}"
    puts Time.now, "END: dedup"
  end

  def self.get_updated
    puts Time.now, "START: removing outdated records"
    # put into temp table all hotels which has 1 status

    self.find_by_sql("DROP TABLE IF EXISTS #{@temp_table}")
    self.find_by_sql("CREATE TABLE #{@temp_table}
                      (hotel_id integer,
                      status integer);")
    puts Time.now, "#{@temp_table} created"

    self.find_by_sql("INSERT INTO #{@temp_table}
                      SELECT hotel_id, max(status) FROM #{@table}
                      GROUP BY hotel_id
                      HAVING count(*)>1;")
    nupdates = self.count_by_sql("SELECT COUNT(*) FROM #{@temp_table};")
    if (nupdates > 0)
      puts Time.now, "#{nupdates} latest records extracted"
      # delete all records from orig table with same hotel_id, but different status

      self.find_by_sql("DELETE FROM #{@table} USING (SELECT * from #{@temp_table})s
                where (s.hotel_id = #{@table}.hotel_id AND
                      s.status <> #{@table}.status);")

      puts Time.now, "OUTDATED RECORDS REMOVED"
    else
      puts "#{@table} has no updated records"
    end

    self.find_by_sql("DROP TABLE IF EXISTS #{@temp_table};")
    puts "dropped #{@temp_table}"
    puts Time.now, "END: removed outdated records"

    self.find_by_sql("DELETE FROM #{@table} where name is null;")
    puts Time.now, "END: deleted hotels with NULL names"

  end

end
