class RedshiftTicket < RedshiftBase
  self.table_name = :ticket

  @table = RedshiftCleanup::Settings.table_names.ticket
  @primary_keys = "
    ticket_id,
    hotel_id,
    status,
    completed_datetime,
    is_hotel,
    resolution_time,
    is_guest,
    contact_name,
    has_met_service_level_agreement,
    severity,
    repair_code,
    location,
    problem_code,
    associate_or_guest,
    ticket_number,
    datetime
  "

  @identity_key = "ticket_id"

  @temp_table = "#{@table}_temp"
  @old_table = "#{@table}_old"

  QUERY_TMPL = <<-EOT
  CREATE TABLE hotel_temp (LIKE hotel);
  BEGIN;
  INSERT INTO hotel_temp (SELECT status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory FROM hotel GROUP BY status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory order by hotel_id asc);
  ALTER TABLE hotel RENAME TO hotel_old;
  ALTER TABLE hotel_temp RENAME TO hotel;
  COMMIT;
  EOT

  def self.cleanup
    # puts QUERY_TMPL
    puts Time.now
    self.find_by_sql("CREATE TABLE IF NOT EXISTS #{@temp_table} (LIKE #{@table});")
    self.find_by_sql("DELETE #{@temp_table};")
    puts "#{@temp_table} created"
    self.find_by_sql("INSERT INTO #{@temp_table} SELECT #{@primary_keys} FROM #{@table} GROUP BY #{@primary_keys} HAVING COUNT(*) > 1;")
    puts Time.now
    ndups = self.count_by_sql("SELECT COUNT(*) FROM #{@temp_table};")
    if (ndups > 0)
      puts "#{ndups} duplicates copied"
      self.find_by_sql("DELETE FROM #{@table} USING (SELECT * FROM #{@temp_table})s 
        WHERE s.ticket_id=#{@table}.ticket_id AND
              s.hotel_id=#{@table}.hotel_id AND
              (s.status=#{@table}.status or (s.status is null AND #{@table}.status is null)) AND
              (s.completed_datetime=#{@table}.completed_datetime or (s.completed_datetime is null AND #{@table}.completed_datetime is null)) AND
              (s.is_hotel=#{@table}.is_hotel or (s.is_hotel is null AND #{@table}.is_hotel is null)) AND
              (s.resolution_time=#{@table}.resolution_time or (s.resolution_time is null AND #{@table}.resolution_time is null)) AND
              (s.is_guest=#{@table}.is_guest or (s.is_guest is null AND #{@table}.is_guest is null)) AND
              (s.contact_name=#{@table}.contact_name or (s.contact_name is null AND #{@table}.contact_name is null)) AND 
              (s.has_met_service_level_agreement=#{@table}.has_met_service_level_agreement or (s.has_met_service_level_agreement is null AND #{@table}.has_met_service_level_agreement is null)) AND
              (s.severity=#{@table}.severity or (s.has_met_service_level_agreement is null AND #{@table}.has_met_service_level_agreement is null)) AND
              (s.repair_code=#{@table}.repair_code or (s.repair_code is null AND #{@table}.repair_code is null)) AND
              (s.location=#{@table}.location or (s.location is null AND #{@table}.location is null)) AND
              (s.problem_code=#{@table}.problem_code or (s.problem_code is null AND #{@table}.problem_code is null)) AND
              (s.associate_or_guest=#{@table}.associate_or_guest or (s.associate_or_guest is null AND #{@table}.associate_or_guest is null)) AND
              (s.ticket_number=#{@table}.ticket_number or (s.ticket_number is null AND #{@table}.ticket_number is null)) AND
              (s.datetime=#{@table}.datetime or (s.datetime is null AND #{@table}.datetime is null))
      ;")
      puts Time.now
      puts "records with duplicates deleted from #{@table}"
      self.find_by_sql("INSERT INTO #{@table}
                        SELECT * from #{@temp_table};")
      Time.now
      puts "unique records re-inserted to #{@table}"

    else
      puts "#{@table} has no duplicates"
    end
    self.find_by_sql("DROP TABLE IF EXISTS #{@temp_table};")
    puts "dropped #{@temp_table}"
    puts Time.now
  end

end
