class RedshiftDeviceSession < RedshiftBase
  self.table_name = :devicesession

  @table = RedshiftCleanup::Settings.table_names.devicesession
  @primary_keys = "
    hotel_id,
    device_session_id,
    enrolment_id,
    mac_address,
    room_name,
    start_time,
    finish_time,
    data_in,
    data_out,
    data_used
  "

  @identity_key = "device_session_id"

  @temp_table = "#{@table}_temp"
  @old_table = "#{@table}_old"

  QUERY_TMPL = <<-EOT
  CREATE TABLE hotel_temp (LIKE hotel);
  BEGIN;
  INSERT INTO hotel_temp (SELECT status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory FROM hotel GROUP BY status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory order by hotel_id asc);
  ALTER TABLE hotel RENAME TO hotel_old;
  ALTER TABLE hotel_temp RENAME TO hotel;
  COMMIT;
  EOT

  def self.cleanup
    # puts QUERY_TMPL
    puts Time.now
    self.find_by_sql("CREATE TABLE IF NOT EXISTS #{@temp_table} (LIKE #{@table});")
    self.find_by_sql("DELETE #{@temp_table};")
    puts "#{@temp_table} created"
    self.find_by_sql("INSERT INTO #{@temp_table} SELECT #{@primary_keys} FROM #{@table} GROUP BY #{@primary_keys} HAVING COUNT(*) > 1;")
    puts Time.now
    ndups = self.count_by_sql("SELECT COUNT(*) FROM #{@temp_table};")
    if (ndups > 0)
      puts "#{ndups} duplicates copied"
      self.find_by_sql("DELETE FROM #{@table} USING (SELECT * FROM #{@temp_table})s 
          WHERE s.hotel_id=#{@table}.hotel_id AND
                s.device_session_id=#{@table}.device_session_id AND
                s.enrolment_id=#{@table}.enrolment_id AND
                (s.mac_address=#{@table}.mac_address or (s.mac_address is null AND #{@table}.mac_address is null)) AND
                (s.room_name=#{@table}.room_name or (s.room_name is null AND #{@table}.room_name is null)) AND
                (s.start_time=#{@table}.start_time or (s.start_time is null AND #{@table}.start_time is null)) AND
                (s.finish_time=#{@table}.finish_time or (s.start_time is null AND #{@table}.start_time is null)) AND
                (s.data_in=#{@table}.data_in or (s.start_time is null AND #{@table}.start_time is null)) AND
                (s.data_out=#{@table}.data_out or (s.data_out is null AND #{@table}.data_out is null)) AND
                (s.data_used=#{@table}.data_used or (s.data_used is null AND #{@table}.data_used is null))
          ; ")
      puts Time.now
      puts "records with duplicates deleted from #{@table}"
      self.find_by_sql("INSERT INTO #{@table}
                        SELECT * from #{@temp_table};")
      Time.now
      puts "unique records re-inserted to #{@table}"
      
    else
      puts "#{@table} has no duplicates"
    end
    self.find_by_sql("DROP TABLE IF EXISTS #{@temp_table};")
    puts "dropped #{@temp_table}"
    puts Time.now
  end

end
