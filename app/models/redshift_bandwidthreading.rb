class RedshiftBandwidthreading < RedshiftBase
  self.table_name = :guest

  @table = RedshiftCleanup::Settings.table_names.bandwidthreading
  @primary_keys = "
    hotel_id,
    date,
    name,
    isp_pool_id,
    ceil_up,
    min_rate_up,
    median_rate_up,
    max_rate_up,
    percentile95th_up,
    brand_standard_met_up,
    ceil_down,
    min_rate_down,
    median_rate_down,
    max_rate_down,
    percentile95th_down,
    brand_standard_met_down
  "

  @identity_key = "guest_id"

  @temp_table = "#{@table}_temp"
  @old_table = "#{@table}_old"

  QUERY_TMPL = <<-EOT
  CREATE TABLE hotel_temp (LIKE hotel);
  BEGIN;
  INSERT INTO hotel_temp (SELECT status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory FROM hotel GROUP BY status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory order by hotel_id asc);
  ALTER TABLE hotel RENAME TO hotel_old;
  ALTER TABLE hotel_temp RENAME TO hotel;
  COMMIT;
  EOT

  def self.cleanup
    
    puts Time.now
    self.find_by_sql("CREATE TABLE IF NOT EXISTS #{@temp_table} (LIKE #{@table});")
    self.find_by_sql("DELETE #{@temp_table};")
    puts "#{@temp_table} created"
    self.find_by_sql("INSERT INTO #{@temp_table} SELECT #{@primary_keys} FROM #{@table} GROUP BY #{@primary_keys} HAVING COUNT(*) > 1;")
    puts Time.now
    ndups = self.count_by_sql("SELECT COUNT(*) FROM #{@temp_table};")
    if (ndups > 0)
      puts "#{ndups} duplicates copied"
      self.find_by_sql("DELETE FROM #{@table} USING (SELECT * FROM #{@temp_table})s 
        WHERE s.hotel_id=#{@table}.hotel_id AND
              s.date=#{@table}.date AND
              (s.name=#{@table}.name or (s.name is null AND #{@table}.name is null)) AND
              (s.isp_pool_id=#{@table}.isp_pool_id or (s.isp_pool_id is null AND #{@table}.isp_pool_id is null)) AND
              s.ceil_up=#{@table}.ceil_up AND
              s.min_rate_up=#{@table}.min_rate_up AND
              s.median_rate_up=#{@table}.median_rate_up AND
              s.max_rate_up=#{@table}.max_rate_up AND
              s.percentile95th_up=#{@table}.percentile95th_up AND
              s.brand_standard_met_up=#{@table}.brand_standard_met_up AND
              s.ceil_down=#{@table}.ceil_down AND
              s.min_rate_down=#{@table}.min_rate_down AND
              s.median_rate_down=#{@table}.median_rate_down AND
              s.max_rate_down=#{@table}.max_rate_down AND
              s.percentile95th_down=#{@table}.percentile95th_down AND
              s.brand_standard_met_down=#{@table}.brand_standard_met_down
        ;")
      puts Time.now
      puts "records with duplicates deleted from #{@table}"
      self.find_by_sql("INSERT INTO #{@table}
                        SELECT * from #{@temp_table};")
      Time.now
      puts "unique records re-inserted to #{@table}"
    else
      puts "#{@table} has no duplicates"
    end
    self.find_by_sql("DROP TABLE IF EXISTS #{@temp_table};")
    puts "dropped #{@temp_table}"
    puts Time.now
  end

end
