class RedshiftUseragent < RedshiftBase
  self.table_name = :useragent

  @table = RedshiftCleanup::Settings.table_names.useragent
  @primary_keys = "
    hotel_id,
    user_agent_id,
    mac_address,
    date,
    device_type,
    device_make,
    device_model,
    device_os,
    device_os_version,
    browser_type,
    browser_version,
    browser_language
  "

  @identity_key = "user_agent_id"

  @temp_table = "#{@table}_temp"
  @old_table = "#{@table}_old"

  QUERY_TMPL = <<-EOT
  CREATE TABLE hotel_temp (LIKE hotel);
  BEGIN;
  INSERT INTO hotel_temp (SELECT status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory FROM hotel GROUP BY status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory order by hotel_id asc);
  ALTER TABLE hotel RENAME TO hotel_old;
  ALTER TABLE hotel_temp RENAME TO hotel;
  COMMIT;
  EOT

  def self.cleanup
    # puts QUERY_TMPL
    puts Time.now
    self.find_by_sql("CREATE TABLE IF NOT EXISTS #{@temp_table} (LIKE #{@table});")
    self.find_by_sql("DELETE #{@temp_table};")
    puts "#{@temp_table} created"
    self.find_by_sql("INSERT INTO #{@temp_table} SELECT #{@primary_keys} FROM #{@table} GROUP BY #{@primary_keys} HAVING COUNT(*) > 1;")
    puts Time.now
    ndups = self.count_by_sql("SELECT COUNT(*) FROM #{@temp_table};")
    if (ndups > 0)
      puts "#{ndups} duplicates copied"
      self.find_by_sql("DELETE FROM #{@table} USING (SELECT * FROM #{@temp_table})s 
        WHERE s.hotel_id=#{@table}.hotel_id AND
              s.user_agent_id=#{@table}.user_agent_id AND
              (s.mac_address=#{@table}.mac_address or (s.mac_address is null AND #{@table}.mac_address is null)) AND
              (s.date=#{@table}.date or (s.date is null AND #{@table}.date is null)) AND
              (s.device_type=#{@table}.device_type or (s.device_type is null AND #{@table}.device_type is null)) AND
              (s.device_make=#{@table}.device_make or (s.device_make is null AND #{@table}.device_make is null)) AND
              (s.device_model=#{@table}.device_model or (s.device_model is null AND #{@table}.device_model is null)) AND
              (s.device_os=#{@table}.device_os or (s.device_os is null AND #{@table}.device_os is null)) AND
              (s.device_os_version=#{@table}.device_os_version or (s.device_os_version is null AND #{@table}.device_os_version is null)) AND
              (s.browser_type=#{@table}.browser_type or (s.browser_type is null AND #{@table}.browser_type is null)) AND
              (s.browser_version=#{@table}.browser_version or (s.browser_version is null AND #{@table}.browser_version is null)) AND
              (s.browser_language=#{@table}.browser_language or (s.browser_language is null AND #{@table}.browser_language is null))
      ;")
      puts Time.now
      puts "records with duplicates deleted from #{@table}"
      self.find_by_sql("INSERT INTO #{@table}
                        SELECT * from #{@temp_table};")
      Time.now
      puts "unique records re-inserted to #{@table}"
    else
      puts "#{@table} has no duplicates"
    end
    self.find_by_sql("DROP TABLE IF EXISTS #{@temp_table};")
    puts "dropped #{@temp_table}"
    puts Time.now
  end

end
