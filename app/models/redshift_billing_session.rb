class RedshiftBillingSession < RedshiftBase
  self.table_name = :billingsession

  @table = RedshiftCleanup::Settings.table_names.billingsession
  @primary_keys = "
    hotel_id,
    billing_session_id,
    enrolment_id,
    units,
    calculation_time,
    billing_type,
    charge
  "

  @identity_key = "billing_session_id"

  @temp_table = "#{@table}_temp"
  @old_table = "#{@table}_old"

  QUERY_TMPL = <<-EOT
  CREATE TABLE hotel_temp (LIKE hotel);
  BEGIN;
  INSERT INTO hotel_temp (SELECT status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory FROM hotel GROUP BY status, name, country, hotel_id, last_infrastructure_upgrade, hotel_group, support_territory order by hotel_id asc);
  ALTER TABLE hotel RENAME TO hotel_old;
  ALTER TABLE hotel_temp RENAME TO hotel;
  COMMIT;
  EOT

  def self.cleanup
    # puts QUERY_TMPL
    puts Time.now
    self.find_by_sql("CREATE TABLE IF NOT EXISTS #{@temp_table} (LIKE #{@table});")
    self.find_by_sql("DELETE #{@temp_table};")
    puts "#{@temp_table} created"
    self.find_by_sql("INSERT INTO #{@temp_table} SELECT #{@primary_keys} FROM #{@table} GROUP BY #{@primary_keys} HAVING COUNT(*) > 1;")
    puts Time.now
    ndups = self.count_by_sql("SELECT COUNT(*) FROM #{@temp_table};")
    if (ndups > 0)
      puts "#{ndups} duplicates copied"
      self.find_by_sql("DELETE FROM #{@table} USING (SELECT * FROM #{@temp_table})s 
          WHERE s.hotel_id=#{@table}.hotel_id and
                s.billing_session_id = #{@table}.billing_session_id and
                s.enrolment_id=#{@table}.enrolment_id and
                s.units=#{@table}.units and
                (s.calculation_time=#{@table}.calculation_time or (s.calculation_time is null AND #{@table}.calculation_time is null)) and
                (s.billing_type=#{@table}.billing_type or (s.billing_type is null AND #{@table}.billing_type is null)) and
                s.charge=#{@table}.charge
          ; ")
      puts Time.now
      puts "records with duplicates deleted from #{@table}"
      self.find_by_sql("INSERT INTO #{@table}
                        SELECT * from #{@temp_table};")
      puts Time.now
      puts "unique records re-inserted to #{@table}"
      
    else
      puts "#{@table} has no duplicates"
    end
    self.find_by_sql("DROP TABLE IF EXISTS #{@temp_table};")
    puts "dropped #{@temp_table}"
    puts Time.now
  end

end
