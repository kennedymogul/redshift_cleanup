Rails.application.secrets.redshift_config = {
  host: RedshiftCleanup::Settings.application.redshift.host,
  port: RedshiftCleanup::Settings.application.redshift.port,
  user: RedshiftCleanup::Settings.application.redshift.user,
  password: RedshiftCleanup::Settings.application.redshift.password,
  database: RedshiftCleanup::Settings.application.redshift.database,
  adapter: 'redshift'
}
